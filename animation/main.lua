function love.load()

    -- load images on frames
    -- frames = {}
    -- for i=1, 5 do
    --     table.insert(frames, love.graphics.newImage("asset/jump" .. i .. ".png"))
    -- end
    

    local frame_width = 117
    local frame_height = 233
    image = love.graphics.newImage("asset/jump.png")
    frames = {}
    for i=0, 4 do
        table.insert(frames, love.graphics.newQuad(i * frame_width, 0, frame_width, frame_height, image:getWidth(), image:getHeight()))
    end

    currentFrame = 1
end

function love.draw()
    -- love.graphics.draw(frames[math.floor(currentFrame)], 200, 200)
    love.graphics.draw(image, frames[math.floor(currentFrame)], 200, 200)
end

function love.update(dt)
    currentFrame =  currentFrame + dt * 10
    if currentFrame > 5 then
        currentFrame = 1
    end
end
