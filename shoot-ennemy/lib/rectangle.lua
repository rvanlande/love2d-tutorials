Rectangle = Object:extend()

function Rectangle:new(x, y, width, height)
    self.x = x
    self.y = y
    self.width = width
    self.height = height
end


function Rectangle:checkCollision(r)

    local a_left = self.x
    local a_right = self.x + self.width
    local a_top = self.y
    local a_bottom = self.y + self.height

    local b_left = r.x
    local b_right = r.x + r.width
    local b_top = r.y
    local b_bottom = r.y + r.height

    return b_left < a_right and b_right > a_left and b_top < a_bottom and b_bottom > a_top
end

function Rectangle:print()
    print(self.x, self.y, self.width, self.height)
end