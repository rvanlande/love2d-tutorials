-- ~ love function ----------------------------------------------

function love.load()
    -- load require
    Object = require "lib/classic"
    require "lib/rectangle"
    require "model/player"
    require "model/ennemy"
    require "model.bullet"

    -- global var
    WINDOW_WIDTH = love.graphics.getWidth()
    WINDOW_HEIGHT = love.graphics.getHeight()

    -- init
    player = Player(WINDOW_WIDTH / 2, 10)
    ennemy = Ennemy(100, WINDOW_HEIGHT - 150)
    bulletCurrentIndex = 1
    bullets = {}
end

function love.update(dt)

    -- update position
    player:update(dt)
    ennemy:update(dt)
    for i, bullet in ipairs(bullets) do
        bullet:update(dt)
        
        local collision = bullet.shape:checkCollision(ennemy.shape)
        if collision then
            ennemy:incSpeed()
        end
        if collision or bullet.y > WINDOW_HEIGHT then
            table.remove(bullets, i)
        end
    end

end

function love.draw()
    player:draw()
    ennemy:draw()
    for i=1, #bullets do
        bullets[i]:draw(dt)
    end
end

function love.keyreleased(key)
    if key == "space" then
       player:shoot()
    end
 end





