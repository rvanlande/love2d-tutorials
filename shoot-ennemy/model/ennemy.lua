Ennemy = Object:extend()

function Ennemy:new(x, y)
    self.x = x
    self.y = y
    self.speed = 100
    self.img = love.graphics.newImage("asset/snake.png")
    self.shape = Rectangle(self.x, self.y, self.img:getWidth(), self.img:getHeight())
end    

function Ennemy:draw()
    love.graphics.draw(self.img, self.x, self.y)
end

function Ennemy:update(dt)

    self.x = self.x + dt * self.speed

    if self.x < 0 then
        self.x = 0
        self.speed = -self.speed
    elseif self.x >= WINDOW_WIDTH - self.img:getWidth() then
        self.x = WINDOW_WIDTH - self.img:getWidth()
        self.speed = -self.speed
    end

    self.shape.x = self.x
end    

function Ennemy:incSpeed()
    if self.speed > 0 then 
        self.speed = self.speed + 50
    elseif self.speed < 0 then
        self.speed = self.speed - 50
    end       
end


