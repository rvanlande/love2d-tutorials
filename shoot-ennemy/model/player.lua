Player = Object:extend()

function Player:new(x, y)
    self.x = x
    self.y = y
    self.speed = 400
    self.img = love.graphics.newImage("asset/panda.png")
end    
 
function Player:draw()
    love.graphics.draw(self.img, self.x, self.y)
end

function Player:update(dt)

    -- player moves
    if love.keyboard.isDown("left") then
        self.x = self.x - self.speed * dt 
    elseif love.keyboard.isDown("right") then
        self.x = self.x + self.speed * dt 
    end

    if self.x < 0 then
        self.x = 0
    elseif self.x > WINDOW_WIDTH - self.img:getWidth() then
        self.x = WINDOW_WIDTH - self.img:getWidth()
    end

end

function Player:shoot()
    bullet = Bullet(self.x + self.img:getWidth() / 2, self.y + self.img:getHeight())
end



