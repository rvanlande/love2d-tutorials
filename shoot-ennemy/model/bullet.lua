Bullet = Object:extend()

function Bullet:new(x, y)
    self.x = x
    self.y = y
    self.speed = 500
    self.img = love.graphics.newImage("asset/bullet.png")
    self.shape = Rectangle(self.x, self.y, self.img:getWidth(), self.img:getHeight())
    
    table.insert(bullets, self)
end

function Bullet:draw()
    love.graphics.draw(self.img, self.x, self.y)
end

function Bullet:update(dt)
    self.y = self.y + dt * self.speed
    self.shape.y = self.y
end

