Wall = Entity:extend()

function Wall:new(x, y)
    Wall.super.new(self, x, y, "asset/wall.png")
    self.type = "Wall"
    self.strength = 100
    self.weight= 0
end