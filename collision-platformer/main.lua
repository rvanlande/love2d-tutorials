function love.load()
    Object = require "lib/classic"
    require "entity"
    require "player"
    require "wall"
    require "box"

    player = Player(100, 100)
    box = Box(400, 150)
    WALL_WIDTH = 50
    WALL_HEIGHT = 50

    objects = {}
    table.insert(objects, player)
    table.insert(objects, box)

    map = {
      {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
      {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
      {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
      {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
      {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
      {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
      {1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1},
      {1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1},
      {1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1},
      {1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1},
      {1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1},
      {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    }

    walls = {}
    for i,row in ipairs(map) do
      for j,wall in ipairs(row) do
        if wall == 1 then
          table.insert(walls, Wall((j-1)*WALL_WIDTH, (i-1)*WALL_HEIGHT))
        end
      end
    end

end

function love.draw()
    for i=1, #objects  do
        objects[i]:draw()
    end
    for i=1, #walls  do
        walls[i]:draw()
    end
end

function love.update(dt)

  for i=1, #objects  do
    objects[i]:update(dt)
  end
  for i=1, #walls  do
    walls[i]:update(dt)
  end

  local loop = true
  local limit = 0

  while loop do

    loop = false
    limit = limit + 1
    if limit > 100 then
      break
    end

    for i=1, #objects-1 do
      for j=i+1, #objects do
        if objects[i]:resolveCollision(objects[j]) then
          loop = true
        end
      end
    end

    for i=1, #walls do
      for j=1, #objects do
        if objects[j]:resolveCollision(walls[i]) then
          loop = true
        end
      end
    end
  end
    
end

function love.keypressed(key)
  if key == 'up' then
    player:jump()
  end
end