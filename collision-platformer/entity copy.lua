-- ~ local functions ------------------------------------------------
local function isRightMove(e)
    return e.x > e.last.x
end

local function isLeftMove(e)
    return e.x < e.last.x
end

local function isUpMove(e)
    return e.y < e.last.y
end

local function isDownMove(e)
    return e.y > e.last.y
end

-- ~ Entity class ---------------------------------------------------------------

Entity = Object:extend()

function Entity:new(x, y, imagePath)
    self.type = "Entity"
    self.x = x
    self.y = y
    self.img = love.graphics.newImage(imagePath)
    self.width = self.img:getWidth()
    self.height = self.img:getHeight()

    self.last = {}
    self.last.x = self.x
    self.last.y = self.y

    self.strength = 0
end

function Entity:draw()
    love.graphics.draw(self.img, self.x, self.y)
end

function Entity:update(dt)
    self.last.x = self.x
    self.last.y = self.y
end

function Entity:resolveCollision(e)

    if not self:isMoving() and not e:isMoving() then
        return
    end

    -- if self.strength == -1 or not self:isMoving() then
    --     e:resolveCollision(self)
    --     return
    -- end
    if self.strength == -1 or self.strength > e.strength then
        e:resolveCollision(self)
        return
    end

    print("check collision : ", self.type, " ", e.type)

    if self:checkCollision(e) then

        if isRightMove(self) and self.last.x + self.width <= e.x then
            if self.strength < e.strength or e.strength == -1 then
                self:alignOnLeft(e)
            else
                self:pushRight(e)
            end
        end

        if isUpMove(self) and self.last.y >= e.y + e.height then
            if self.strength < e.strength or e.strength == -1 then
                self:alignOnBottom(e)
            else
                self:pushUp(e)
            end
        end

        if isLeftMove(self) and self.last.x >= e.x + e.width then
            if self.strength < e.strength or e.strength == -1 then
                self:alignOnRight(e)
            else
                self:pushLeft(e)
            end
        end

        if isDownMove(self) and self.last.y + self.height <= e.y then
            if self.strength < e.strength or e.strength == -1 then
                self:alignOnTop(e)
            else
                self:pushDown(e)
            end
        end
        
    end
end

function Entity:checkCollision(e)
    local a_left = self.x
    local a_right = self.x + self.width
    local a_top = self.y
    local a_bottom = self.y + self.height

    local b_left = e.x
    local b_right = e.x + e.width
    local b_top = e.y
    local b_bottom = e.y + e.height

    return b_left < a_right and b_right > a_left and b_top < a_bottom and b_bottom > a_top
end

function Entity:isMoving()
    return self.last.x ~= self.x or self.last.y ~= self.y
end

function Entity:alignOnTop(e)
    self.y = e.y - self.height
end

function Entity:alignOnBottom(e)
    self.y = e.y + e.height
end

function Entity:alignOnLeft(e)
    self.x = e.x - self.width
end

function Entity:alignOnRight(e)
    self.x = e.x + e.width
end

function Entity:pushRight(e)
    e.x = self.x + self.width
end

function Entity:pushDown(e)
    e.y = self.y + self.height
end

function Entity:pushLeft(e)
    e.x = self.x - e.width
end

function Entity:pushUp(e)
    e.y = self.y - e.height
end

-- function Entity:resolveCollision(e)

--     if self:checkCollision(e) then

--         if isRightDownMove(self) then

--             if self.last.x + self.width <= e.last.x then
--                 self:alignOnLeft(e)
--             else
--                 self:alignOnTop(e)
--             end

--         elseif self:isRightUpMove() then

--             if self.last.x + self.width <= e.last.x then
--                 self:alignOnLeft(e)
--             else
--                 self:alignOnBottom(e)
--             end

--         elseif self:isLeftDownMove() then

--             if self.last.x >= e.last.x + e.width then
--                 self:alignOnRight(e)
--             else
--                 self:alignOnTop(e)
--             end

--         elseif self:isLeftUpMove() then

--             if self.last.x >= e.last.x + e.width then
--                 self:alignOnRight(e)
--             else
--                 self:alignOnBottom(e)
--             end
--         end
        
--     end
-- end



-- function Entity:isRightDownMove()
--     return self.x >= self.last.x and self.y >= self.last.y
-- end

-- function Entity:isRightUpMove()
--     return self.x >= self.last.x and self.y <= self.last.y
-- end

-- function Entity:isLeftDownMove()
--     return self.x <= self.last.x and self.y >= self.last.y
-- end

-- function Entity:isLeftUpMove()
--     return self.x <= self.last.x and self.y <= self.last.y
-- end