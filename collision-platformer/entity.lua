-- ~ Entity class ---------------------------------------------------------------
Entity = Object:extend()

local entitySequenceId = 0

function Entity:new(x, y, imagePath)
    self.type = "Entity"
    entitySequenceId = entitySequenceId + 1
    self.id = entitySequenceId
    self.x = x
    self.y = y
    self.img = love.graphics.newImage(imagePath)
    self.width = self.img:getWidth()
    self.height = self.img:getHeight()

    self.last = {}
    self.last.x = self.x
    self.last.y = self.y

    self.strength = 0
    self.tempStrength = 0

    self.gravity = 0
    self.weight = 400
end

function Entity:draw()
    love.graphics.draw(self.img, self.x, self.y)
end

function Entity:update(dt)
    self.last.x = self.x
    self.last.y = self.y
    self.tempStrength = self.strength
    self.gravity = self.gravity + dt * self.weight
    self.y = self.y + self.gravity * dt
end

function Entity:checkCollision(e)
    local a_left = self.x
    local a_right = self.x + self.width
    local a_top = self.y
    local a_bottom = self.y + self.height

    local b_left = e.x
    local b_right = e.x + e.width
    local b_top = e.y
    local b_bottom = e.y + e.height

    return b_left < a_right and b_right > a_left and b_top < a_bottom and b_bottom > a_top
end

function Entity:resolveCollision(e)

    if self.tempStrength > e.tempStrength then
        return e:resolveCollision(self)
    end

    if self:checkCollision(e) then

        if self:wasHorizontallyAligned(e) then

            if self:getCenterPos().x < e:getCenterPos().x then
                self:collideRight(e)
            else
                self:collideLeft(e)
            end

        elseif self:wasVerticallyAligned(e) then

            if self:getCenterPos().y < e:getCenterPos().y then
                self:collideBottom(e)
            else
                self:collideTop(e)
            end
            
        end

        self.tempStrength = e.tempStrength        
        return true
    end

    return false
end

function Entity:wasHorizontallyAligned(e)
    return self.last.y + self.height > e.last.y and self.last.y < e.last.y + e.height
end

function Entity:wasVerticallyAligned(e)
    return self.last.x + self.width > e.last.x and self.last.x < e.last.x + e.width
end


function Entity:getCenterPos()
    return {
        x = self.x + self.width / 2,
        y = self.y + self.height / 2,
    }
end

function Entity:collideBottom(e)
    self.y = e.y - self.height
    self.gravity = 0
end

function Entity:collideTop(e)
    self.y = e.y + e.height
end

function Entity:collideRight(e)
    self.x = e.x - self.width
end

function Entity:collideLeft(e)
    self.x = e.x + e.width
end

function Entity:toString()
    return self.type .. "-" .. self.id .. "-" .. self.tempStrength
end