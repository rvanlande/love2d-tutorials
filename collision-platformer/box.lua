Box = Entity:extend()

function Box:new(x, y)
    Box.super.new(self, x, y, "asset/box.png")
    self.type = "Box"
    self.strength = 10
end