Player = Entity:extend()

function Player:new(x, y)
    Player.super.new(self, x, y, "asset/player.png")
    self.type = "Player"
    self.speed = 300
    self.strength = 50
    self.canJump = false
end

function Player:update(dt)

    Player.super.update(self, dt)

    if love.keyboard.isDown("left") then
        self.x = self.x - dt * self.speed
    elseif love.keyboard.isDown("right") then
        self.x = self.x + dt * self.speed
    end

    if self.last.y ~= self.y then
        self.canJump = false
    end

end

function Player:jump()
    if self.canJump then
        self.gravity = -300
        self.canJump = false
    end
end

function Player:collideBottom(e)
    Player.super.collideBottom(self, e)
    self.canJump = true
end
