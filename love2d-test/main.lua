
function love.load()

    Object = require "lib.classic"
    require "subdir.example"
    require "models.rectangle"
    tick = require "lib.tick"

    love.graphics.setBackgroundColor(0, 0, 0)

    wWidth = love.graphics.getWidth()
    wHeight = love.graphics.getHeight()
    speed = 200
    x = wWidth / 2
    y = wHeight / 2
    fruits = { "apple", "banana"}
    table.insert(fruits, "pear")

    for i = 1, #fruits do
        print("I eat ", fruits[i])
    end

    drawTickMsg = false
    tick.delay(function () drawTickMsg = true end, 2)

    r1 = Rectangle(100, 100, 10, 50)
    r2 = Rectangle(200, 100, 10, 50)

    sheepImg = love.graphics.newImage("assets/img/sheep.png")
end    

function love.draw()

    love.graphics.setColor(1, 1, 1)
    printWindowSize()
    love.graphics.rectangle("line", x, y, 10, 50)
    love.graphics.print(fruits[1], 10, 20)
    
    if drawTickMsg then
        love.graphics.print("Yeaahh tick message", 10, 30)
    end

    r1:draw()
    r2:draw()

    love.graphics.setColor(1, 1, 0)
    love.graphics.draw(sheepImg, wWidth - 100, 100, math.pi / 2, 2, 2, sheepImg:getWidth() / 2, sheepImg:getHeight() / 2)
end

function love.update(dt)

    tick.update(dt)

    if x < wWidth - 10 and love.keyboard.isDown("right") then
        x = x + speed * dt
    elseif x > 0 and love.keyboard.isDown("left") then
        x = x - speed * dt
    elseif y > 0 and love.keyboard.isDown("up") then
        y = y - speed * dt
    elseif y < wHeight - 50 and love.keyboard.isDown("down") then
        y = y + speed * dt
    end

    r1:update(dt)
    r2:update(dt)
    
end    

-- ~ utils function -------------------------------------------------------

function printWindowSize()
    windowSize =  wWidth .. "x" .. wHeight
    love.graphics.print("Window size => " .. windowSize, 10, 10)
    
end    
