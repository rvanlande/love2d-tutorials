function love.load()

    circle = {}
    circle.x = 200
    circle.y = 200
    circle.radius = 25
    circle.speed = 200
    cursorImg = love.graphics.newImage("arrow_right.png")

end

function love.draw()
    love.graphics.print("angle : " .. angle, 10, 10)
    -- love.graphics.line(circle.x, circle.y, mouseX, circle.y)
    -- love.graphics.line(mouseX, circle.y, mouseX, mouseY)
    -- love.graphics.line(circle.x, circle.y, mouseX, mouseY)    
    -- love.graphics.circle("line", circle.x, circle.y, distance)
    love.graphics.draw(cursorImg, circle.x, circle.y, angle, 1, 1, cursorImg:getWidth() / 2, cursorImg:getHeight() / 2)
    love.graphics.circle("fill", mouseX, mouseY, 10)
end

function love.update(dt)

    mouseX, mouseY = love.mouse.getPosition()
    distance = getDistance(circle.x, circle.y, mouseX, mouseY)

    angle = math.atan2(mouseY - circle.y, mouseX - circle.x)
    circle.x = circle.x + dt * circle.speed * math.cos(angle) * (distance / 100)
    circle.y = circle.y + dt * circle.speed * math.sin(angle) * (distance / 100)
    
end

function getDistance(x1, y1, x2, y2)

    local _a = x1 - x2
    local _b = y1 - y2

    return math.sqrt(_a ^2 + _b^2)
end
