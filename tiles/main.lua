function love.load()

    tilemapOriginX = 100
    tilemapOriginY = 100
    tilemap = {
        {1, 6, 6, 2, 1, 6, 6, 2},
        {3, 0, 0, 4, 5, 0, 0, 3},
        {3, 0, 0, 0, 0, 0, 0, 3},
        {4, 2, 0, 0, 0, 0, 1, 5},
        {1, 5, 0, 0, 0, 0, 4, 2},
        {3, 0, 0, 0, 0, 0, 0, 3},
        {3, 0, 0, 1, 2, 0, 0, 3},
        {4, 6, 6, 5, 4, 6, 6, 5}
    }

    -- load image and create quads
    tilesetImg = love.graphics.newImage("tileset.png")
    tileWidth = (tilesetImg:getWidth() / 3) - 2
    tileHeight = (tilesetImg:getHeight() / 2) - 2
    quads = {}
    for i = 0, 1 do
        for j = 0, 2 do
            local _quad = love.graphics.newQuad(j * tileWidth, i * tileHeight, tileWidth, tileHeight, tilesetImg:getWidth(), tilesetImg:getHeight())
            table.insert( quads, _quad)
        end
    end

    -- player
    player = {
        img = love.graphics.newImage("player.png"),
        tileX = 2,
        tileY = 2
    }
        
end

function love.draw()

    for i, row in ipairs(tilemap) do
        for j, v in ipairs(row) do
            if v ~= 0 then
                love.graphics.draw(
                    tilesetImg,
                    quads[v],
                    tilemapOriginX + (j-1) * tileWidth,
                    tilemapOriginY + (i-1) * tileHeight
                )
            end
        end
    end

    love.graphics.draw(player.img, tilemapOriginX + player.tileX * tileWidth, tilemapOriginY + player.tileY * tileHeight)
end

function love.keypressed(key)

    local _oldX = player.tileX
    local _oldY = player.tileY

    if key == "left" then
        player.tileX = player.tileX - 1
    elseif key == "right" then
        player.tileX = player.tileX + 1
    elseif key == "up" then
        player.tileY = player.tileY - 1
    elseif key == "down" then
        player.tileY = player.tileY + 1
    end

    if tilemap[player.tileY + 1][player.tileX + 1] > 0 then
        player.tileX = _oldX
        player.tileY = _oldY
    end
end

