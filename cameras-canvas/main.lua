function love.load()

    math.randomseed(os.time())
    speed = 200

    player = {
        x = 200,
        y = 100,
        size = 25,
        img = love.graphics.newImage("face.png")
    }

    coins = {}
    imgCoin = love.graphics.newImage("dollar.png")
    for i=1,25 do
        table.insert( coins, { 
            x = math.random( 50, 650),
            y = math.random( 50, 450),
            size = 10,
            img = imgCoin
        })
    end

    score = 0
end

function love.draw()

    love.graphics.push()
    love.graphics.translate(-player.x + 400, -player.y + 300)

    -- draw player
    love.graphics.circle("line", player.x, player.y, player.size)
    love.graphics.draw(player.img, player.x, player.y, 0, 1, 1, player.img:getWidth() / 2, player.img:getHeight() / 2)

    -- draw coins
    for i=1,#coins do
        love.graphics.circle("line", coins[i].x, coins[i].y, coins[i].size)
        love.graphics.draw(coins[i].img, coins[i].x, coins[i].y, 0, 1, 1, coins[i].img:getWidth() / 2, coins[i].img:getHeight() / 2)
    end

    love.graphics.pop()
    
    love.graphics.print("Score = " .. score, 10, 10)

end

function love.update(dt)
    
    if love.keyboard.isDown("left") then
        player.x = player.x - speed * dt
    elseif love.keyboard.isDown("right") then
        player.x = player.x + speed * dt
    end

    if love.keyboard.isDown("up") then
        player.y = player.y - speed * dt
    elseif love.keyboard.isDown("down") then
        player.y = player.y + speed * dt
    end

    for i=#coins,1,-1 do
        if checkCollision(player, coins[i]) then
            table.remove(coins, i)
            player.size = player.size + 1
            score = score + 1
        end
    end
end

function checkCollision(p1, p2)
    local _distance = math.sqrt((p1.x - p2.x)^2 + (p1.y - p2.y)^2)
    return _distance < p1.size + p2.size
end